<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/login-external/{user}/{password}', 'LoginExternalController@attempt')->name('login.external');
Route::get('/login', 'CustomloginController@showLoginForm')->name('login');
Route::post('/login', 'CustomloginController@login');
Route::get('/logout', 'CustomloginController@logout');
Route::get('/login-custom', 'CustomloginController@login')->name('login.custom');
Route::get('/logout-custom', 'CustomloginController@logout')->name('logout.custom');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index')->middleware('auth:custom');

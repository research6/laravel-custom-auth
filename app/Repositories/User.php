<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models;

class User
{
    /**
     * @return \App\Models\User
     */
    public static function detail(): Models\User
    {
        return new Models\User([
            'token' => session()->get('token'),
            'profile' => session()->get('profile')
        ]);
    }
}

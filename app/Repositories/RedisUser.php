<?php


namespace App\Repositories;

use App\Models\Redis\User;
use Illuminate\Support\Facades\Redis;

class RedisUser
{
    public $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @return User
     */
    public function find($identifier)
    {
        return Redis::get($this->model->table);
    }

    /**
     * @return User
     */
    public function store($identifier, $data)
    {
        return Redis::set($this->model->table, $data);
    }

}

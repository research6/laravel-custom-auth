<?php declare(strict_types=1);

namespace App\Services\Auth;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as AuthProvider;
use App\Repositories;

use Illuminate\Validation\ValidationException;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use App\Models\User;

class UserProvider implements AuthProvider
{

    public function retrieveById($identifier): Authenticatable
    {
        return Repositories\User::detail();
    }

    public function retrieveByToken($identifier, $token)
    {

    }

    public function updateRememberToken(Authenticatable $user, $token)
    {

    }

    public function retrieveByCredentials(array $credentials)
    {

        if (empty($credentials) || (count($credentials) === 1 && Str::contains($this->firstCredentialKey($credentials), 'password'))) {
            return;
        }

        try {

            $client = new Client();

            $form = array(
                'grant_type' => 'password',
                'client_id' => 5,
                'client_secret' => 'pQQVo6PY9mPL90Gkxv2FJnrHSOoOnlMxQ7yuRZy3',
                'username' => 'aaaaaa' . $credentials['username'],
                'password' => $credentials['password']
            );

            $dispatch = $client->post(env('API_BASE_URL').'/oauth/token', [
                'connect_timeout' => 5,
                'timeout' => 25,
                'form_params' => $form
            ]);

            $decode = json_decode($dispatch->getBody()->getContents());

        } catch (RequestException $e) {
            if ($e instanceof ClientException) {

                return;

            } else if ($e instanceof ConnectException) {


                throw ValidationException::withMessages([
                    'loginexception' => ['Connection time out.'],
                ]);

            } else {

                throw ValidationException::withMessages([
                    'loginexception' => ['Something when wrong, Please Try Again.'],
                ]);

            }

        } catch (\Exception $e) {

            throw ValidationException::withMessages([
                'loginexception' => ['Something when wrong, Please Try Again.'],
            ]);

        }

        return (isset($decode->access_token)) ? new User([
            'token' => $decode->access_token
        ]) : null;
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {

        $client  = new Client();
        $dispatch = $client->request('GET', env('API_BASE_URL').'/api/v1/users/me', [
            'connect_timeout'   => 5,
            'timeout'           => 25,
            'headers' => [
                'Authorization' => 'Bearer '.$user->token
            ]
        ]);

        $decode = json_decode($dispatch->getBody()->getContents());

        if (!isset($decode->data))
            return false;

        session(['token' => $user->token]);
        session(['profile' => $decode->data->profile]);

        return true;

    }
}

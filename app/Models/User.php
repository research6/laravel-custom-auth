<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable;
    /**
     * @var array
     */
    protected $guarded = ['custom'];

    protected $fillable = ['token', 'profile'];

    public function getAuthIdentifierName()
    {
        return 'token';
    }

    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }
}

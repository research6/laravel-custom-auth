<?php

namespace App\Models\Redis;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable;
    /**
     * @var array
     */

    public $table = 'user';

    public function getKeyName()
    {
        return 'id';
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;

use App\Models\User;
use Illuminate\Support\Facades\Auth as BaseAuth;


class LoginExternalController extends Controller
{
    /**
     * @param string $user
     * @param string $password
     *
     * @return \App\Models\User
     */
    public function attempt(string $user, string $password)
    {
      $client = new Client();

    		$form = array(
    			'grant_type'    => 'password',
    			'client_id'     => 5,
    			'client_secret' => 'pQQVo6PY9mPL90Gkxv2FJnrHSOoOnlMxQ7yuRZy3',
    			'username' => $user,
    			'password' => $password
    		);

    		$dispatch = $client->post('http://gp-api.krakenteamdev.com/oauth/token',[
    			'connect_timeout'	=> 5,
    			'timeout'			=> 25,
    			'form_params' => $form
    		]);

    	$decode = json_decode($dispatch->getBody()->getContents());

        $response['user']['token'] = $decode->access_token;

        static::auth(new User($response['user']));

        return redirect()->route('dashboard.index');
    }

    /**
     * @param \App\Models\User $user
     *
     * @return \App\Models\User
     */
    protected static function auth(User $user): User
    {
        static::session($user);
        static::bind($user);
        static::login($user);

        return $user;
    }

    /**
     * @param \App\Models\User $user
     *
     * @return void
     */
    protected static function session(User $user): void
    {
        session(['token' => $user->token]);
    }

    /**
     * @param \App\Models\User $user
     *
     * @return void
     */
    protected static function bind(User $user): void
    {
        // app()->bind('user', static function () use ($user): User {
        //     return $user;
        // });

        app()->bind('token', static function (): string {
            return session('token');
        });
    }

    /**
     * @param \App\Models\User $user
     *
     * @return void
     */
    protected static function login(User $user): void
    {
        BaseAuth::guard('custom')->login($user, true);
    }
}

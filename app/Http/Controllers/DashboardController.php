<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        Redis::set('name', 'jihan');
        Redis::get('name');
        dd(Redis::lrange('names', 5, 10));
        // dd(Auth::user());
        //dd('dashboard');

        return view('home');
    }
}

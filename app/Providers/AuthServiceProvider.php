<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Services\Auth\CustomGuard;
use App\Services\Auth\UserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

         Auth::extend('custom', function ($app, $name, array $config) {
            return new CustomGuard(Auth::createUserProvider($config['provider']), $app->make('request'));
           
        });

         Auth::provider('custom', function ($app, array $config) {
             return new UserProvider();
        });
    }
}
